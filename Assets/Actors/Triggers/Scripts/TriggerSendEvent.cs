using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerSendEvent : MonoBehaviour
{
    [SerializeField]
    UnityEvent onTrigger;

    [SerializeField]
    LayerMask layerMask;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (((layerMask.value & (1 << collision.gameObject.layer)) > 0))
        {
            onTrigger.Invoke();
        }
    }
}
