using System;
using UnityEngine;
using Zenject;


public class RespawnManagerInstaller : MonoInstaller
{
    [SerializeField]
    GameObject defaultSpawn;
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        Container.DeclareSignal<SpawnPlayer>();
        Container.DeclareSignal<SetNewSpawn>();
        Container.BindInterfacesTo<RespawnManager>().AsSingle().WithArguments(defaultSpawn).NonLazy();
    }
}

public class RespawnManager : IInitializable, IDisposable
{
    readonly SignalBus signalBus;

    GameObject spawn;

    public RespawnManager(SignalBus signalBus, GameObject defaultSpawn)
    {
        this.signalBus = signalBus;
        spawn = defaultSpawn;
    }

    public void Initialize()
    {
        signalBus.Subscribe<SpawnPlayer>(OnSpawnPlayer);
        signalBus.Subscribe<SetNewSpawn>(OnSetNewRespawnPoint);
    }

    public void Dispose()
    {
        signalBus.Unsubscribe<SpawnPlayer>(OnSpawnPlayer);
        signalBus.Unsubscribe<SetNewSpawn>(OnSetNewRespawnPoint);
    }

    public void OnSetNewRespawnPoint(SetNewSpawn args)
    {
        spawn = args.Spawn;
    }

    public void OnSpawnPlayer(SpawnPlayer args)
    {
        args.Player.transform.position = spawn.transform.position; // todo methode teleport
    }
}